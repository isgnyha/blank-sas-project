# Overview

Week 3's example code.

## Organization of the folders 

### Top level folder

This is where the main code for the project resides. The programs will be numbered sequentially in the order in which they should be executed. There may be few exceptions where a file needs to be run iteratively in relation to a subsequent one.

### Folder: support

This folder has some code to set up working environment (e.g. reading the lookup tables, and running preliminary tasks like DRG grouping etc.).

## Description of the files

### 0. Initialize.sas

Sets up the librefs, and runs preliminary code from the "support" folder to set up utility and convenience macros. This should be run once after SAS starts (and after the file changes).
