/*************************       MACRO :   BD           ***********************/
/* %bd(bin);                                                                  */
/* Macro to convert a Binary value to Decimal value                           */
/* Note: This is veryu seful when working with the _TYPE_ variable created    */
/*       by several SAS PROCs (especially if there are many CLASS variables.  */
/*  Argument                                                                  */
/*		bin : Binary value to be converted to decimal value.                  */
/*  Example:                                                                  */
/*      %bin(1101);                                                           */
/******************************************************************************/
%macro bd(bin);
	%local len dec i;
	%let len=%length(&bin);
	%let dec=0;	
	%do i=1 %to &len;
		%let dec=&dec+%substr(&bin,&i,1)*%eval((2**(&len-&i)));
	%end;
	%eval(&dec)
%mend;
/******************************************************************************/



/*********************       MACRO :   LOOKUP_ERROR        ********************/
/* Display the error number for unexpected errors for an indexed lookup.      */
/* Note:                                                                      */
/*     The semicolon should be omitted when calling this macro from the       */
/*     within a SELECT loop, as the semicolon is already provided by the      */
/*     DO loop. Using the semicolon will immediately terminate the WHEN or    */
/*     the OTHERWISE statements.                                              */
/* Example:                                                                   */
/*      select (expression);                                                  */
/*          when (condition) do;                                              */
/*              "stuff";                                                      */
/*          end;                                                              */
/*          otherwise %lookup_error                                           */
/*      end;                                                                  */
/******************************************************************************/
%macro lookup_error();
	do;
		put 'ERROR: Unexpected value for _IORC_= ' _iorc_;
		put 'Program terminating. Data step iteration # ' _n_;
		put _all_;
		stop;
	end;
%mend;
/******************************************************************************/



/*************************       MACRO :   CSV_FILE     ***********************/
/* Macro for CSV file creation: There is an updated version (to be debugged)  */
/******************************************************************************/
%macro csv_file(inp_file,dat_file);

%let prg_file=%sysfunc(sysget(temp))\efi.sas;  /* This one requires that the TEMP environment variable is set */
%let fmt = best19.;

proc contents data=&inp_file noprint out=work._inpv1_;
run;

proc sort data=work._inpv1_ out=work._inpv2_ (keep=type name);
	by varnum;
*	where name in ('OPCERT' 'CASE_NO' 'COST_C' 'DRG2' 'LOS');
run;

data _null_;
	set work._inpv2_ end=islast;
	file "&dat_file" lrecl=32767;
	put name $ +(-1) @;
	if not islast then put ',' @;
	else put;
run;

data _null_;
	file "&prg_file" lrecl=32767;
	set work._inpv2_ end=islast;
	if _n_=1 then do;
		put "data _null_;";
		put "    length _efitmp_ $200;";
		put "    file '&dat_file' mod lrecl=32767;";
		put "    set &inp_file;";
		num_n  = 0;
		char_n = 0;
	end;
	num_n  + (type=1);
	char_n + (type=2);
	if islast then do;
		call symput('num_n',put(num_n,5.));
		call symput('char_n',put(char_n,5.));
	end;
run;

data _null_;
	file "&prg_file" mod lrecl=32767;
	set work._inpv2_ end=islast;
	num_n  = &num_n;
	char_n = &char_n;
	ctr+1;
	if type = 1 then do;
		if ctr=1 then do;
			put "    if " name "  > .z then do;";
			put "        _efitmp_ = left(put(" name ", &fmt ));";
			put "        put _efitmp_ $ +(-1) @;";
			put "    end;";
		end;
		else  do;
			put "    if " name "  > .z then do;";
			put "        _efitmp_ = left(put(" name ", &fmt ));";
			put "        put ',' _efitmp_ $ +(-1) @;";
			put "    end;";
			put "    else put ',' @; "; /* missing */
		end;
	end;
	if type = 2 then do;
		if ctr=1 then do;
			put "    if " name  " ^=' ' then do;";
			put "        if indexc(" name  ", '"",') > 0 then _efitmp_= put(" name ", $quote200.);";
			put "        else _efitmp_ = put(" name ", $200.);";
			put "        put _efitmp_ $ +(-1) @;";
			put "    end;";
		end;
		else do; 
			put "    if " name  " =' ' then put ',' @;"; /* missing */
			put "    else do;";
			put "        if indexc(" name  ", '"",') > 0 then _efitmp_= put(" name ", $quote200.);";
			put "        else _efitmp_ = put(" name ", $200.);";
			put "        put ',' _efitmp_ $ +(-1) @;";
			put "    end;";
		end;
	end;
	if islast then do;
		put "    put;"; /* Move to the next line */
		put "run;";
	end;
run;

%include "&prg_file";	
%mend;	
/******************************************************************************/
